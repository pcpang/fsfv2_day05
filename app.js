/**
 * Created by pohchye on 1/7/2016.
 */

var express = require("express");
// for post method
var bodyParser = require("body-parser");

var app = express();
// for post method
app.use(bodyParser.urlencoded({'extended': 'true'}));
app.use(bodyParser.json());

//Middleware
app.post("/register", function (req, res) {
    //get method
    // var username = req.query.username;
    // var email = req.query.email;
    // var gender = req.query.gender;
    // post methid
    var username = req.body.params.username;
    var email = req.body.params.email;
    var gender = req.body.params.gender;
    console.info("Server username = %s", username);
    console.info("Server email = %s", email);
    console.info("Server gender = %s", gender);
    console.info("res = %s", res);
    res.status(200).end();
});

app.get("/thankyou", function (req, res) {
    res.redirect("thankyou.html");
});

app.get("/showNames", function (req, res) {
    var names = [
        {"firstname": "John", "lastname": "Smith"},
        {"firstname": "Anna", "lastname": "Doe"},
        {"firstname": "Peter", "lastname": "Jones"}
    ];
    res.json(names);
    // age = [1,2,3,4,5]
    // res.json({
    //     name: names,
    //     age: age
    // });
});

app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/bower_components"));

app.use(function (req, res, next) {
    res.redirect("error.html");
});

app.set("port",
    process.argv[2] || process.env.APP_PORT || 3005);

app.listen(app.get("port"), function () {
        console.info("Express server started on port %s", app.get("port"))
    }
);
