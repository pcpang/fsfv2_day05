var RegApp = angular.module("RegApp", []);

(function () {
    var RegCtrl;

    RegCtrl = function ($http, $window) {
        var ctrl = this;
        ctrl.username = "";
        ctrl.email = "";
        ctrl.gender = "";
        ctrl.names = [];
        $http.get("/showNames")
            .success(function (a) {
                console.info(a);
                ctrl.names = a;
            })
            .error(function () {
                console.log("Some error occured")
            });


        //console.info("Client username = %s", ctrl.username);
        //console.info("Client email = %s", ctrl.email);
        // console.info("Client gender = %s", ctrl.gender);
        ctrl.register = function () {
            console.info("Client username = %s", ctrl.username);
            console.info("Client email = %s", ctrl.email);
            console.info("Client gender = %s", ctrl.gender);
            $http.post("/register", {
                params: {
                    username: ctrl.username,
                    email: ctrl.email,
                    gender: ctrl.gender
                }
            }).then(function () {
                console.info("Success");
                //$window.location = "/thankyou";
            }).catch(function () {
                console.error("ERROR");
            });

        };
    };

    RegApp.controller("RegCtrl", ["$http", "$window", RegCtrl]);
})();